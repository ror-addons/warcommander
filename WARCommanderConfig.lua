--------------------------------------------------------------------------------
-- Copyright (c) 2009 Flimgoblin <fingoniel@roguishness.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      WARCommanderConfig.lua
-- Date:      2009-06-10T15:01:00Z
-- Author:    Flimgoblin <fingoniel@roguishness.com>
-- Version:   Beta v0.6
-- Copyright: 2009
--------------------------------------------------------------------------------

local editorWindow = "WARCommander_ConfigWindow"
local editorMode = CLEAN_MODE
local T = LibStub("WAR-AceLocale-3.0"):GetLocale("WARCommander")

local function SetComboBox( comboBox, selection )
	if comboBox == "SubType" then
		ComboBoxSetSelectedMenuItem(editorWindow .. comboBox .. "ComboBox", selection)
		LabelSetText(editorWindow .. comboBox .. "Label", towstring(subTypeList[selection].id) )
	end
end

local channelNames={}	
local channelIds={}
local channelPositions={}

local function PopulateChannelList(channelType, channels,set)
	ComboBoxClearMenuItems(editorWindow .. channelType.."ComboBox")
	local counter = 0
	for i, v in pairs(channels) do
		counter = counter + 1
		ComboBoxAddMenuItem(editorWindow .. channelType.."ComboBox", towstring(v))					
	end
	ComboBoxSetSelectedMenuItem(editorWindow .. channelType.."ComboBox",channelPositions[set])
end

WARCommanderConfig={}

function WARCommanderConfig.Init()
	CreateWindow( editorWindow, false )	
	LabelSetText(editorWindow .. "TitleBarText", 	T[L"WARCommander Configuration"])
	LabelSetText(editorWindow .. "RVRAlertsLabel", 	T[L"Alerts Channel (RvR):"])
	LabelSetTextColor(editorWindow.. "RVRAlertsLabel",0,255,0)	
	LabelSetText(editorWindow .. "RVRCommandsLabel", 	T[L"Command Channel (RvR):"])
	LabelSetTextColor(editorWindow.. "RVRCommandsLabel",0,255,0)	
	LabelSetText(editorWindow .. "ScenAlertsLabel", 	T[L"Alerts Channel (Scen):"])	
	LabelSetTextColor(editorWindow.. "ScenAlertsLabel",255,204,153)
	LabelSetText(editorWindow .. "ScenCommandsLabel", 	T[L"Command Channel (Scen):"])	
	LabelSetTextColor(editorWindow.."ScenCommandsLabel",255,204,153)
	
	LabelSetText(editorWindow .. "ListenCheckBoxLabel", 	T[L"Listen for WARCommander commands/alerts on:"])	
	
	LabelSetText(editorWindow .. "OtherOptionsLabel", 	T[L"Other Options:"])	
	LabelSetText(editorWindow .. "WaypointCheckBoxLabel", 	T[L"Autocreate Waypoints"])	
	ButtonSetCheckButtonFlag(editorWindow .. "WaypointCheckBox", true)		
	ButtonSetPressedFlag(editorWindow .. "WaypointCheckBox",WARCommander_config.setWaypoints)	
		
	LabelSetText(editorWindow .. "UpdatesCheckBoxLabel", 	T[L"Show Updates Window"])
	ButtonSetCheckButtonFlag(editorWindow .. "UpdatesCheckBox", true)		
	ButtonSetPressedFlag(editorWindow .. "UpdatesCheckBox",not WARCommander_config.hideInfo)
		
	LabelSetText(editorWindow .. "UpdatesTitleCheckBoxLabel", 	T[L"Show Updates Title"])
	ButtonSetCheckButtonFlag(editorWindow .. "UpdatesTitleCheckBox", true)		
	ButtonSetPressedFlag(editorWindow .. "UpdatesTitleCheckBox",not WARCommander_config.hideInfoTitle)
	local count=0	
	for k,v in pairs(WARCommander.listen_channels) do
		count=count +1
		table.insert(channelNames,WARCommander.valid_channels[k])
		table.insert(channelIds,k)	
		channelPositions[k]=count
		
		
		local xpos=(count-1)%3
		local ypos=math.floor((count-1)/3)				
		local checkBoxWindow="WARCommanderConfigListenCheckbox"..v
		CreateWindowFromTemplate(checkBoxWindow, "WARCommanderConfig_ListenCheckBoxTemplate", editorWindow)
		WindowClearAnchors(checkBoxWindow)				
		WindowAddAnchor(checkBoxWindow, "topleft", editorWindow.."LeftColHeader", "topleft", 15+(xpos*200), 180+(ypos*40))
		
		
		local checkBoxWindowLabel="WARCommanderConfigListenCheckboxLabel"..v	
		CreateWindowFromTemplate(checkBoxWindowLabel, "WARCommanderConfig_ListenCheckBoxLabelTemplate", editorWindow)
		WindowClearAnchors(checkBoxWindowLabel)				
		WindowAddAnchor(checkBoxWindowLabel, "right", checkBoxWindow, "left", 10,0)
		LabelSetText(checkBoxWindowLabel,T[towstring(WARCommander.valid_channels[k])])
		ButtonSetCheckButtonFlag(checkBoxWindow, true)						
		if(WARCommander_config.listening[v]==1) then
			ButtonSetPressedFlag(checkBoxWindow,true)			
		end
		WindowSetShowing(checkBoxWindow, true)				
		WindowSetShowing(checkBoxWindowLabel, true)				
	end
	PopulateChannelList("RVRAlerts",channelNames,WARCommander_config.alertchannel)
	PopulateChannelList("RVRCommands",channelNames,WARCommander_config.channel)
	PopulateChannelList("ScenAlerts",channelNames,WARCommander_config.scalertchannel)
	PopulateChannelList("ScenCommands",channelNames,WARCommander_config.scchannel)
end

function WARCommanderConfig.ShowConfigWindow() WindowSetShowing(editorWindow,true) end
function WARCommanderConfig.OnRVRCommandsChange(selected) WARCommander_config.channel=channelIds[selected] end
function WARCommanderConfig.OnRVRAlertsChange(selected)	WARCommander_config.alertchannel=channelIds[selected] end
function WARCommanderConfig.OnScenCommandsChange(selected) WARCommander_config.scchannel=channelIds[selected] end
function WARCommanderConfig.OnScenAlertsChange(selected) WARCommander_config.scalertchannel=channelIds[selected] end

function WARCommanderConfig.OnListenCheckBoxChanged()
	local window=SystemData.MouseOverWindow.name
	local chan=window:match("WARCommanderConfigListenCheckbox(.*)")
	if (chan) then
		if(ButtonGetPressedFlag(window)) then
			WARCommander.ListenChannel(tonumber(chan))
		else
			WARCommander.BlockChannel(tonumber(chan))
		end		
	end
end

function WARCommanderConfig.OnWaypointCheckBoxChanged() WARCommander_config.setWaypoints = ButtonGetPressedFlag(SystemData.MouseOverWindow.name) end

function WARCommanderConfig.OnUpdatesCheckBoxChanged()
	local window=SystemData.MouseOverWindow.name
	if(ButtonGetPressedFlag(window)) then
		WARCommander.ShowInfo()
	else
		WARCommander.HideInfo()
	end
end

function WARCommanderConfig.OnUpdatesTitleCheckBoxChanged()
	local window=SystemData.MouseOverWindow.name
	if(ButtonGetPressedFlag(window)) then
		WARCommander.ShowInfoTitle()
	else
		WARCommander.HideInfoTitle()
	end
end

function WARCommanderConfig.OnShown() end
function WARCommanderConfig.OnHidden() end
function WARCommanderConfig.Close() WindowSetShowing(editorWindow,false) end